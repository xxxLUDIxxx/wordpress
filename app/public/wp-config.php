<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YRXIksEqHtaDlGHt6n85IGyvXOXJfXP36kGT/+eXfd0Dpy3lJ1UuZTBv/yndt+eqivIs8obTkUf9LsDiMjwd/Q==');
define('SECURE_AUTH_KEY',  'Rh7cGhalwFVZH8ReoU+oXyz/7GBgzIJ7j0W07Q1b9s5NR0eiXL6wSR4HGDPXHvZI1ibcX/PWWBwa42kNYqAkzg==');
define('LOGGED_IN_KEY',    'NIypNG8eNTooOs6A9pWa+hQRfFrRh6alNXY1H9Gng5MK31PKnhvMsIARyfJQGNtFbL47hnlltNZ+8LqDfP+FCQ==');
define('NONCE_KEY',        'SkbEVl6LC7U33AMFOm11N5bHI9Kis2S3QH0eSkRKsB5S3t1vsuTLmmAvq6agOXw/psdwqARdftqO12Rco1TUvg==');
define('AUTH_SALT',        '3eLqRvpW0WvT9n3zs80v/ZH42H7i0bQVUa3DZ5OHUB0XeuzoG0rFjNrQO+kKUG/wNrZsRGiNScazpumqIjZuJw==');
define('SECURE_AUTH_SALT', 'eanhtR1ipCwICDzequqbz3DU38oGG3OHTsUOyUkQVvkOt6/jnpoqD07y1vd0gMHnZYZURQcUtOruca6K281cXQ==');
define('LOGGED_IN_SALT',   'wxLp7EfZpgfrRa4Yyz+veRg7d3OSgD1mzGVJj+MdFhXSkWfv1zBGUZ4+e++PPfaqiVdbip1zR6aQJFCgIf27qg==');
define('NONCE_SALT',       'AU1mEQz82Vc5IAtO9fkrBE0QC5pu9W/MZnlB80djs1BG7Q9MAL/JG9wLS1rI0o/NZpkp8RjlDN2HVSuf/TGwaQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
